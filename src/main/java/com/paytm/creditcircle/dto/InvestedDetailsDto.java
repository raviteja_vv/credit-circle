package com.paytm.creditcircle.dto;

import lombok.Data;

import java.time.LocalDate;

@Data
public class InvestedDetailsDto {
  private Long loanId;

  private String userName;

  private String amount;

  private Double roi;

  private Integer tenure;

  private LocalDate startDate;

  private LocalDate endDate;

  private Long principleReceived;

  private Long principleOutstanding;

  private Long interestEarned;
}
