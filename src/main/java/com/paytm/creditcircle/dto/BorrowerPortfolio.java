package com.paytm.creditcircle.dto;

import lombok.Data;

import java.util.List;

@Data
public class BorrowerPortfolio {
  private Long totalAmount;

  private Long principleReceived;

  private Long principleRemaining;

  private Long interestPaid;

  private UpcomingEMIDetailsDto upcomingEmiDetailsDto;

  private List<BorrowedDetailsDto> borrowedDetailsDtoList;

}
