package com.paytm.creditcircle.dto;

import lombok.Data;

import java.time.LocalDate;

@Data
public class BorrowedDetailsDto {
  private Long loanId;

  private String userName;

  private String amount;

  private Double roi;

  private Integer tenure;

  private LocalDate startDate;

  private LocalDate endDate;

  private Integer emiDaysLeft;

  private Long emiAmount;

  private Long principleReceived;

  private Long principleOutstanding;

  private Long interestPaid;

}
