package com.paytm.creditcircle.dto;

import lombok.Data;

import java.util.List;

@Data
public class InvestorPortfolio {
  private Long totalAmount;

  private Long principleReceived;

  private Long principleOutstanding;

  private Long interestEarned;

  private List<InvestedDetailsDto> investedDetailsDtoList;
}
