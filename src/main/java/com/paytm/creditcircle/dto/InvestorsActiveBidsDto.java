package com.paytm.creditcircle.dto;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

@Data
public class InvestorsActiveBidsDto {
  private Long loanId;

  private String userName;

  private String amount;

  private Double minRoi;

  private Double maxRoi;

  private Integer tenure;

  @DateTimeFormat(pattern = "yyyy-MM-dd")
  private LocalDate expiry;

  private String status;

  private String label;
}
