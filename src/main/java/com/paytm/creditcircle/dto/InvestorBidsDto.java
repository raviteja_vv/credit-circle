package com.paytm.creditcircle.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.util.List;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class InvestorBidsDto {
  private Long loanId;

  private String amount;

  private Double minRoi;

  private Double maxRoi;

  private Integer tenure;

  @DateTimeFormat(pattern = "yyyy-MM-dd")
  private LocalDate expiry;

  private boolean visibility;

  private List<InvestorsReceivedBidsDto> investorsReceivedBidsDtoList;
}
