package com.paytm.creditcircle.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class InvestorsReceivedBidsDto {

  private String userName;

  private String amount;

  private Double roi;

  private Integer tenure;

  private LocalDate expiry;

  private String creditScore;

  private String status;
}
