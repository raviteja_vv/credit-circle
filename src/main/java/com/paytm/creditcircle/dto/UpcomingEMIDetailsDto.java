package com.paytm.creditcircle.dto;

import lombok.Data;

@Data
public class UpcomingEMIDetailsDto {
  private Long loanId;

  private String userName;

  private Long amount;

  private Double roi;

}
