package com.paytm.creditcircle.service;

import com.paytm.creditcircle.dto.*;
import com.paytm.creditcircle.utils.Constants;
import org.springframework.stereotype.Service;

import java.text.DecimalFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static com.paytm.creditcircle.utils.Constants.borrowerBidsListCache;

@Service
public class BorrowerService {

  public List<InvestorsActiveBidsDto> fetchAllInvestorsBids() {
    Random random = new Random();
    double[] amountList = random.doubles(Constants.investorsActiveBidsCount, 1, 50).toArray();
    int[] nameIndexList = random.ints(Constants.investorsActiveBidsCount, 0, Constants.namesList.size()).toArray();
    long[] loanIdList = random.longs(Constants.investorsActiveBidsCount, 1000000000L, 10000000000L).toArray();
    double[] minRoiList = random.doubles(Constants.investorsActiveBidsCount, 8, 15).toArray();
    double[] maxRoiList = random.doubles(Constants.investorsActiveBidsCount, 15, 25).toArray();
    int[] tenureList = random.ints(Constants.investorsActiveBidsCount, 3, 36).toArray();
    int[] expiryDateList = random.ints(Constants.investorsActiveBidsCount, 1, 31).toArray();
    int[] bidStatusList = random.ints(Constants.investorsActiveBidsCount, 0, Constants.borrowerBidStatusList.size()).toArray();
    int[] bidLabelList = random.ints(Constants.investorsActiveBidsCount, 1, Constants.borrowerBidLabelList.size()).toArray();
    bidLabelList[0] = 0;

    DecimalFormat decimalFormat = new DecimalFormat("#.00");

    List<InvestorsActiveBidsDto> investorsActiveBidsDtoList = new ArrayList<>();
    investorsActiveBidsDtoList.addAll(borrowerBidsListCache);
    for(int i = 0; i < Constants.investorsActiveBidsCount; i++) {
      InvestorsActiveBidsDto investorsActiveBidsDto = new InvestorsActiveBidsDto();
      investorsActiveBidsDto.setLoanId(loanIdList[i]);
      investorsActiveBidsDto.setUserName(Constants.namesList.get(nameIndexList[i]));
      investorsActiveBidsDto.setAmount(decimalFormat.format(amountList[i]) + " lac");
      investorsActiveBidsDto.setMinRoi(Double.parseDouble(decimalFormat.format(minRoiList[i])));
      investorsActiveBidsDto.setMaxRoi(Double.parseDouble(decimalFormat.format(maxRoiList[i])));
      investorsActiveBidsDto.setTenure(tenureList[i]);
      investorsActiveBidsDto.setExpiry(LocalDate.now().plusDays(expiryDateList[i]));
      investorsActiveBidsDto.setStatus(Constants.borrowerBidStatusList.get(bidStatusList[i]));
      investorsActiveBidsDto.setLabel(Constants.borrowerBidLabelList.get(bidLabelList[i]));
      investorsActiveBidsDtoList.add(investorsActiveBidsDto);
      borrowerBidsListCache.add(investorsActiveBidsDto);
    }

    return investorsActiveBidsDtoList;
  }

  public BorrowerPortfolio fetchBorrowerPortfolio() {
    Random random = new Random();
    double[] amountList = random.doubles(Constants.borrowerPortfolioCount, 1, 30).toArray();
    int[] nameIndexList = random.ints(Constants.borrowerPortfolioCount, 0, Constants.namesList.size()).toArray();
    long[] loanIdList = random.longs(Constants.borrowerPortfolioCount, 1000000000L, 10000000000L).toArray();
    double[] roiList = random.doubles(Constants.borrowerPortfolioCount, 8, 25).toArray();
    int[] tenureList = random.ints(Constants.borrowerPortfolioCount, 3, 36).toArray();
    int[] startDateList = random.ints(Constants.borrowerPortfolioCount, -31, 0).toArray();
    long[] interestPaidList = random.longs(Constants.borrowerPortfolioCount, 100, 5000).toArray();
    int[] emiDaysLeftList = random.ints(Constants.borrowerPortfolioCount, 2, 20).toArray();
    long[] emiAmountList = random.longs(Constants.borrowerPortfolioCount, 10000, 40000).toArray();

    DecimalFormat decimalFormat = new DecimalFormat("#.00");

    long totalAmount = 0, totalPrincipleReceived = 0, totalPrincipleRemaining = 0, totalInterestPaid = 0;
    int minEmiDaysLeft = 100, upcomingEmiId = 0;
    List<BorrowedDetailsDto> borrowedDetailsDtoList = new ArrayList<>();
    for(int i = 0; i < Constants.borrowerPortfolioCount; i++) {
      BorrowedDetailsDto borrowedDetailsDto = new BorrowedDetailsDto();
      borrowedDetailsDto.setLoanId(loanIdList[i]);
      borrowedDetailsDto.setUserName(Constants.namesList.get(nameIndexList[i]));
      double loanAmountInLakhs = Double.parseDouble(decimalFormat.format(amountList[i]));
      long loanAmount = (long) (loanAmountInLakhs * 100000);
      borrowedDetailsDto.setAmount(loanAmountInLakhs + " lac");
      borrowedDetailsDto.setRoi(Double.parseDouble(decimalFormat.format(roiList[i])));
      borrowedDetailsDto.setTenure(tenureList[i]);
      borrowedDetailsDto.setStartDate(LocalDate.now().plusDays(startDateList[i]));
      borrowedDetailsDto.setEndDate(LocalDate.now().plusDays(startDateList[i]).plusMonths(tenureList[i]));
      borrowedDetailsDto.setEmiDaysLeft(emiDaysLeftList[i]);
      borrowedDetailsDto.setEmiAmount(emiAmountList[i]);
      if(emiDaysLeftList[i] < minEmiDaysLeft) {
        minEmiDaysLeft = emiDaysLeftList[i];
        upcomingEmiId = i;
      }

      long principleReceived = (long) (Math.random() * ((loanAmount - 100000L) / 10));
      borrowedDetailsDto.setPrincipleReceived(principleReceived);
      borrowedDetailsDto.setPrincipleOutstanding((loanAmount - principleReceived));
      borrowedDetailsDto.setInterestPaid(interestPaidList[i]);
      totalPrincipleReceived += principleReceived;
      totalPrincipleRemaining += (loanAmount - principleReceived);
      totalInterestPaid += interestPaidList[i];
      totalAmount += loanAmount;
      borrowedDetailsDtoList.add(borrowedDetailsDto);
    }

    BorrowedDetailsDto upcomingEMIDto = borrowedDetailsDtoList.get(upcomingEmiId);
    UpcomingEMIDetailsDto upcomingEMIDetailsDto = new UpcomingEMIDetailsDto();
    upcomingEMIDetailsDto.setLoanId(upcomingEMIDto.getLoanId());
    upcomingEMIDetailsDto.setAmount(upcomingEMIDto.getEmiAmount());
    upcomingEMIDetailsDto.setUserName(upcomingEMIDto.getUserName());
    upcomingEMIDetailsDto.setRoi(upcomingEMIDto.getRoi());

    BorrowerPortfolio borrowerPortfolio = new BorrowerPortfolio();
    borrowerPortfolio.setTotalAmount(totalAmount);
    borrowerPortfolio.setPrincipleReceived(totalPrincipleReceived);
    borrowerPortfolio.setPrincipleRemaining(totalPrincipleRemaining);
    borrowerPortfolio.setInterestPaid(totalInterestPaid);
    borrowerPortfolio.setBorrowedDetailsDtoList(borrowedDetailsDtoList);
    borrowerPortfolio.setUpcomingEmiDetailsDto(upcomingEMIDetailsDto);
    return borrowerPortfolio;
  }

  public void addInvestorBids(InvestorsActiveBidsDto investorsActiveBidsDto) {
    borrowerBidsListCache.add(investorsActiveBidsDto);
  }
}
