package com.paytm.creditcircle.service;

import com.paytm.creditcircle.dto.InvestedDetailsDto;
import com.paytm.creditcircle.dto.InvestorBidsDto;
import com.paytm.creditcircle.dto.InvestorPortfolio;
import com.paytm.creditcircle.dto.InvestorsReceivedBidsDto;
import com.paytm.creditcircle.utils.Constants;
import org.springframework.stereotype.Service;

import java.text.DecimalFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static com.paytm.creditcircle.utils.Constants.*;

@Service
public class InvestorService {

  public List<InvestorBidsDto> fetchInvestorsBidsList() {
    Random random = new Random();
    double[] amountList = random.doubles(Constants.investorBidsCount, 1, 50).toArray();
    long[] loanIdList = random.longs(Constants.investorBidsCount, 1000000000L, 10000000000L).toArray();
    double[] minRoiList = random.doubles(Constants.investorBidsCount, 8, 15).toArray();
    double[] maxRoiList = random.doubles(Constants.investorBidsCount, 15, 25).toArray();
    int[] tenureList = random.ints(Constants.investorBidsCount, 3, 36).toArray();
    int[] expiryDateList = random.ints(Constants.investorBidsCount, 1, 31).toArray();

    DecimalFormat decimalFormat = new DecimalFormat("#.00");

    List<InvestorBidsDto> investorsBidsDtoList = new ArrayList<>();
    investorsBidsDtoList.addAll(investorBidsListCache);
      for(int i = 0; i < Constants.investorBidsCount; i++) {
      InvestorBidsDto investorsBidsDto = new InvestorBidsDto();
      investorsBidsDto.setLoanId(loanIdList[i]);
      investorsBidsDto.setAmount(decimalFormat.format(amountList[i]) + " lac");
      investorsBidsDto.setMinRoi(Double.parseDouble(decimalFormat.format(minRoiList[i])));
      investorsBidsDto.setMaxRoi(Double.parseDouble(decimalFormat.format(maxRoiList[i])));
      investorsBidsDto.setTenure(tenureList[i]);
      investorsBidsDto.setExpiry(LocalDate.now().plusDays(expiryDateList[i]));
      investorsBidsDto.setVisibility(random.nextBoolean());
      investorsBidsDtoList.add(investorsBidsDto);
    }

    return investorsBidsDtoList;
  }

  public List<InvestorBidsDto> fetchBidsRequestedByBorrower() {
    Random random = new Random();
    double[] amountList = random.doubles(Constants.requestedBidsLoanCount, 1, 50).toArray();
    long[] loanIdList = random.longs(Constants.requestedBidsLoanCount, 1000000000L, 10000000000L).toArray();
    double[] minRoiList = random.doubles(Constants.requestedBidsLoanCount, 8, 15).toArray();
    double[] maxRoiList = random.doubles(Constants.requestedBidsLoanCount, 16, 25).toArray();
    int[] tenureList = random.ints(Constants.requestedBidsLoanCount, 4, 36).toArray();
    int[] expiryDateList = random.ints(Constants.requestedBidsLoanCount, 1, 31).toArray();

    DecimalFormat decimalFormat = new DecimalFormat("#.00");

    List<InvestorBidsDto> investorsBidsDtoList = new ArrayList<>();
    for(int i = 0; i < Constants.requestedBidsLoanCount; i++) {
      InvestorBidsDto investorsBidsDto = new InvestorBidsDto();
      double amount = Double.parseDouble(decimalFormat.format(amountList[i]));
      double minRoi = Double.parseDouble(decimalFormat.format(minRoiList[i]));
      double maxRoi = Double.parseDouble(decimalFormat.format(maxRoiList[i]));
      investorsBidsDto.setLoanId(loanIdList[i]);
      investorsBidsDto.setAmount(amount + " lac");
      investorsBidsDto.setMinRoi(minRoi);
      investorsBidsDto.setMaxRoi(maxRoi);
      investorsBidsDto.setTenure(tenureList[i]);
      investorsBidsDto.setExpiry(LocalDate.now().plusDays(expiryDateList[i]));
      investorsBidsDto.setVisibility(random.nextBoolean());

      int bidsCount = (int) (Math.random() * Constants.perLoanBidsCount) + 1;

      double[] receivedAmountList = random.doubles(bidsCount, 1, amount).toArray();
      double[] roiList = random.doubles(bidsCount, minRoi, maxRoi).toArray();
      int[] nameIndexList = random.ints(Constants.investedPortfolioCount, 0, Constants.namesList.size()).toArray();
      int[] receivedTenureList = random.ints(bidsCount, 3, tenureList[i]).toArray();
      int[] creditScoreList = random.ints(bidsCount, 0, Constants.creditScoreList.size()).toArray();
      int[] bidStatusList = random.ints(bidsCount, 0, Constants.requestedBidStatusList.size()).toArray();

      List<InvestorsReceivedBidsDto> investorsReceivedBidsDtoList = new ArrayList<>();
      for(int j = 0; j < bidsCount; j++) {
        InvestorsReceivedBidsDto investorsReceivedBidsDto = new InvestorsReceivedBidsDto();
        investorsReceivedBidsDto.setUserName(Constants.namesList.get(nameIndexList[j]));
        investorsReceivedBidsDto.setAmount(decimalFormat.format(receivedAmountList[j]) + " lac");
        investorsReceivedBidsDto.setRoi(Double.parseDouble(decimalFormat.format(roiList[j])));
        investorsReceivedBidsDto.setTenure(receivedTenureList[j]);
        investorsReceivedBidsDto.setCreditScore(Constants.creditScoreList.get(creditScoreList[j]));
        investorsReceivedBidsDto.setStatus(Constants.requestedBidStatusList.get(bidStatusList[j]));
        investorsReceivedBidsDtoList.add(investorsReceivedBidsDto);
      }

      investorsBidsDto.setInvestorsReceivedBidsDtoList(investorsReceivedBidsDtoList);
      investorsBidsDtoList.add(investorsBidsDto);
    }

    return investorsBidsDtoList;
  }

  public InvestorPortfolio fetchInvestorPortfolio() {
    Random random = new Random();
    double[] amountList = random.doubles(Constants.investedPortfolioCount, 1, 30).toArray();
    int[] nameIndexList = random.ints(Constants.investedPortfolioCount, 0, Constants.namesList.size()).toArray();
    long[] loanIdList = random.longs(Constants.investedPortfolioCount, 1000000000L, 10000000000L).toArray();
    double[] roiList = random.doubles(Constants.investedPortfolioCount, 8, 25).toArray();
    int[] tenureList = random.ints(Constants.investedPortfolioCount, 3, 36).toArray();
    int[] startDateList = random.ints(Constants.investedPortfolioCount, -31, 0).toArray();
    long[] principleOutstandingList = random.longs(Constants.investedPortfolioCount, 0, 1000).toArray();
    long[] interestEarnedList = random.longs(Constants.investedPortfolioCount, 100, 5000).toArray();

    DecimalFormat decimalFormat = new DecimalFormat("#.00");

    long totalAmount = 0, totalPrincipleReceived = 0, totalPrincipleOutstanding = 0, totalInterestEarned = 0;
    List<InvestedDetailsDto> investedDetailsDtoList = new ArrayList<>();
    for(int i = 0; i < Constants.investedPortfolioCount; i++) {
      InvestedDetailsDto investedDetailsDto = new InvestedDetailsDto();
      investedDetailsDto.setLoanId(loanIdList[i]);
      investedDetailsDto.setUserName(Constants.namesList.get(nameIndexList[i]));
      double loanAmountInLakhs = Double.parseDouble(decimalFormat.format(amountList[i]));
      long loanAmount = (long) (loanAmountInLakhs * 100000);
      investedDetailsDto.setAmount(loanAmountInLakhs + " lac");
      investedDetailsDto.setRoi(Double.parseDouble(decimalFormat.format(roiList[i])));
      investedDetailsDto.setTenure(tenureList[i]);
      investedDetailsDto.setStartDate(LocalDate.now().plusDays(startDateList[i]));
      investedDetailsDto.setEndDate(LocalDate.now().plusDays(startDateList[i]).plusMonths(tenureList[i]));

      long principleReceived = (long) (Math.random() * ((loanAmount - 100000L) / 10));
      investedDetailsDto.setPrincipleReceived(principleReceived);
      investedDetailsDto.setPrincipleOutstanding(principleOutstandingList[i]);
      investedDetailsDto.setInterestEarned(interestEarnedList[i]);
      totalPrincipleReceived += principleReceived;
      totalPrincipleOutstanding += principleOutstandingList[i];
      totalInterestEarned += interestEarnedList[i];
      totalAmount += loanAmount;
      investedDetailsDtoList.add(investedDetailsDto);
    }

    InvestorPortfolio investorPortfolio = new InvestorPortfolio();
    investorPortfolio.setTotalAmount(totalAmount);
    investorPortfolio.setPrincipleReceived(totalPrincipleReceived);
    investorPortfolio.setPrincipleOutstanding(totalPrincipleOutstanding);
    investorPortfolio.setInterestEarned(totalInterestEarned);
    investorPortfolio.setInvestedDetailsDtoList(investedDetailsDtoList);
    return investorPortfolio;
  }

  public void addInvestorBids(InvestorBidsDto investorBidsDto) {
    investorBidsListCache.add(investorBidsDto);

  }

  public InvestorsReceivedBidsDto bidderDetails() {

    Double amount =Math.random() * 6.0;

    return InvestorsReceivedBidsDto.builder().userName(Constants.namesList.get((int)(Math.random()*(namesList.size()))))
            .amount(amount.toString() + " lacs").creditScore(creditScoreList.get((int)(Math.random()*3)))
            .expiry(LocalDate.now().plusDays(5)).roi((Math.random()*(17))+8).status("PENDING").tenure(4)
            .build();
  }
}
