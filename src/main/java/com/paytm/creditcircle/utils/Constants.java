package com.paytm.creditcircle.utils;

import com.paytm.creditcircle.dto.InvestorBidsDto;
import com.paytm.creditcircle.dto.InvestorsActiveBidsDto;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Constants {

  public static final Integer investorBidsCount = 10;
  public static final Integer requestedBidsLoanCount = 2;
  public static final Integer perLoanBidsCount = 4;
  public static final Integer investedPortfolioCount = 3;
  public static final  List<InvestorBidsDto> investorBidsListCache = new ArrayList<>();

  public static final  List<InvestorsActiveBidsDto> borrowerBidsListCache = new ArrayList<>();
  public static final List<String> namesList = Arrays.asList(
    "Aaryan", "Abdul", "Abraham", "Ajay", "Alex", "Aman", "Anil", "Aryan", "Bharath", "Charly", "Daksh", "Dravid",
    "Edward", "Emanuel", "Farhan", "George", "Gopi", "Harikrishna", "Harleen", "Harish", "Harris", "Harry", "Hussain",
    "Ibrahim", "Jacky", "Jacob", "Jai", "James", "John", "Johnathan", "Kelvin", "Kohli", "Lauren", "Lohith", "Malik",
    "Manish", "Manisha", "Micheal", "Mikey", "Morgan", "Muhammad", "Nikhil", "Nikita", "Noor", "Naman", "Olivia",
    "Omar", "Owais", "Pardeepraj", "Parkash", "Prakhar", "Paul", "Prince", "Prithvi", "Pushkar", "Qasim", "Ravi",
    "Rahul", "Rahim", "Rahman", "Raja", "Ricky", "Rikki", "Rishi", "Robert", "Rohan", "Rohini", "Rohit", "Ronald",
    "Russel", "Sahil", "Saif", "Salman", "Sam", "Samir", "Samuel", "Sailaja", "Sandeep", "Sanjay", "Shreyas", "Shyam",
    "Siddharth", "Sohail", "Sultan", "Suraj", "Surya", "Talib", "Tanmay", "Taylor", "Thomas", "Tony", "Umar", "Usman",
    "Victor", "Vinay", "Vincent", "William", "Xavier", "Youssef", "Zakir"
  );

  public static final List<String> requestedBidStatusList = Arrays.asList(
    "PENDING", "APPROVED", "PENDING", "APPROVED", "PENDING", "REJECTED", "PENDING", "PENDING", "REJECTED", "PENDING"
  );

  public static final List<String> creditScoreList = Arrays.asList(
    "LOW", "MID", "HIGH"
  );

  public static final Integer investorsActiveBidsCount = 20;

  public static final Integer borrowerPortfolioCount = 2;

  public static final List<String> borrowerBidStatusList = Arrays.asList(
    "PENDING", "PENDING", "PENDING", "PENDING_FOR_APPROVAL", "PENDING", "PENDING", "ACCEPTED", "PENDING", "PENDING", "PENDING"
  );

  public static final List<String> borrowerBidLabelList = Arrays.asList(
    "Top Investor", "", "New", "", "", "", "", "NEW", "", ""
  );


}
