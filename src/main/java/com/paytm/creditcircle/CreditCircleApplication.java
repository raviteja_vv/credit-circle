package com.paytm.creditcircle;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CreditCircleApplication {

	public static void main(String[] args) {
		SpringApplication.run(CreditCircleApplication.class, args);
	}

}
