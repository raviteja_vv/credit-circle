package com.paytm.creditcircle.controller;

import com.paytm.creditcircle.dto.InvestorBidsDto;
import com.paytm.creditcircle.dto.InvestorPortfolio;
import com.paytm.creditcircle.dto.InvestorsReceivedBidsDto;
import com.paytm.creditcircle.service.InvestorService;
import com.paytm.creditcircle.utils.Constants;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController()
@RequestMapping("/investor")
@AllArgsConstructor
public class InvestorController {

  private final InvestorService investorService;

  @GetMapping("/bids")
  public List<InvestorBidsDto> fetchInvestorsBidsList() {
    return investorService.fetchInvestorsBidsList();
  }

  @GetMapping("/receivedBids")
  public List<InvestorBidsDto> fetchBidsRequestedByBorrower() {
    return investorService.fetchBidsRequestedByBorrower();
  }

  @GetMapping("/portfolio")
  public InvestorPortfolio fetchInvestorPortfolio() {
    return investorService.fetchInvestorPortfolio();
  }

  @PostMapping("/addBids")
  public ResponseEntity addInvestorBids( @ModelAttribute InvestorBidsDto investorBidsDto){
    investorService.addInvestorBids(investorBidsDto);
    return ResponseEntity.ok("Success");
  }

  @PostMapping("/bidder/details")
  public InvestorsReceivedBidsDto fetchBidderDetails() {
    return investorService.bidderDetails();
  }

}
