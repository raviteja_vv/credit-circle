package com.paytm.creditcircle.controller;

import com.paytm.creditcircle.dto.BorrowerPortfolio;
import com.paytm.creditcircle.dto.InvestorBidsDto;
import com.paytm.creditcircle.dto.InvestorsActiveBidsDto;
import com.paytm.creditcircle.service.BorrowerService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController()
@RequestMapping("/borrower")
@AllArgsConstructor
public class BorrowerController {

  private final BorrowerService borrowerService;

  @GetMapping("/bids")
  public List<InvestorsActiveBidsDto> fetchAllInvestorsBids() {
    return borrowerService.fetchAllInvestorsBids();
  }

  @GetMapping("/portfolio")
  public BorrowerPortfolio fetchBorrowerPortfolio() {
    return borrowerService.fetchBorrowerPortfolio();
  }

  @PostMapping("/addBids")
  public ResponseEntity addInvestorBids(@ModelAttribute InvestorsActiveBidsDto InvestorsActiveBidsDto){
    borrowerService.addInvestorBids(InvestorsActiveBidsDto);
    return ResponseEntity.ok("Success");
  }

  @GetMapping("/pendingBids")
  public List<InvestorsActiveBidsDto> fetchAllAcceptedBids(){

    return null;
  }
}
